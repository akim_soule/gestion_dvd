package exception;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * Classe permettant de lancer une alerte a la suite d'une mauvaise entree de l'utilisateur.
 * Elle affiche un message personnalise.
 * @author akimsoule
 * @version 1.0
 */
public class lanceralerte{
	
	/**
	 * Principal constructeur de la classe.
	 * Elle a pour principals attributs le titre et le message de la fenetre.
	 * @param titre
	 * Le titre de la fenetre.
	 * @param msg
	 * Le message de la fenetre.
	 */
	public lanceralerte (String titre, String msg) {
		Alert alert = new Alert(AlertType.ERROR, msg);
		alert.setHeaderText(titre);
		alert.showAndWait();
	}

}
