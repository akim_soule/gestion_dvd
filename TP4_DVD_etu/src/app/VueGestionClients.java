package app;

import humain.Client;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 * Cette classe correspond a la vueGestionClient.
 * Elle contient les formulaires pour l'ajout et la modification d'un client.
 * @author akimsoule.
 * @version 1.0.
 *
 */
public class VueGestionClients {
	/**
	 * Le root de la vue.
	 */
	private BorderPane bPaneGestionClient = new BorderPane();
	
	/**
	 * Les textes de la vue.
	 */
	private Text listeDesClientText, modificationDunClientText, ajoutDunClientText;
	
	/**
	 * La labels de la vue.
	 */
	private Label numeroDeClientLabel, nomLabel1, prenomLabel1, sexeLabel1, ageLabel1, statutLabel1, nomLabel2,
	prenomLabel2, sexeLabel2, ageLabel2;
	private Label labelClientList;
	
	/**
	 * Les TextField de la vue.
	 */
	private TextField numeroDuClientTextField, nomTextField1, prenomTextField1, sexeTextField1, ageTextField1, statutTextField1, nomTextField2,
	prenomTextField2, sexeTextField2, ageTextField2;
	
	/**
	 * Les bouttons radio de la vue.
	 */
	private RadioButton hommeButtonRadio1, hommeButtonRadio2, femmeButtonRadio1, femmeButtonRadio2, envigueurButtonRadio, suspenduButtonRadio;
	
	/**
	 * Les boutons afficherClient, modifierClient, et ajouterClient
	 */
	private Button affichierUnClientButton, modifierClientButton, ajouterClientButton;
	
	/**
	 * Les groupes de radioBouton
	 */
	private ToggleGroup sexeGroup1, sexeGroup2, statutGroup;

	

	/**
	 * Constructeur principal de la classe permettant de mettre en place les parties gauche, centre et droite.
	 */
	public VueGestionClients() {
		
		VBox VGauche = new VBox();
		VBox VCentre = new VBox();
		VBox VDroite = new VBox();
		setListeDesClients(VGauche);
		setModificationDunClient(VCentre);
		setAjoutDunClient(VDroite);
		
		VGauche.setPrefSize(350, 400);
		VCentre.setMinSize(300, 400);
		VDroite.setMinSize(300, 400);
		
		bPaneGestionClient.setLeft(VGauche);
		bPaneGestionClient.setCenter(VCentre);
		bPaneGestionClient.setRight(VDroite);
		
		bPaneGestionClient.setPadding(new Insets(4));
		
		VGauche.setPadding(new Insets(0, 20, 0, 0));
		VCentre.setPadding(new Insets(0, 20, 0, 0));
		
		
		
		/**
		 * Boucle permettant de faire la mise en forme de la partie centre.
		 */
		for (int i = 0; i < VCentre.getChildren().size(); i++) {
			
			if (VCentre.getChildren().get(i).getClass().equals(Label.class)) {
				((Labeled) VCentre.getChildren().get(i)).setFont(Font.font("Arial", FontWeight.NORMAL, 13));
			}
			if (VCentre.getChildren().get(i).getClass().equals(RadioButton.class)) {
				
				((RadioButton) VCentre.getChildren().get(i)).setFont(Font.font("Arial", FontWeight.NORMAL, 13));
			}
			if (VCentre.getChildren().get(i).getClass().equals(Text.class)) {
				
				if( ((Text)VCentre.getChildren().get(i)).getText().equals("\n")) {
					
					((Text)VCentre.getChildren().get(i)).setFont(Font.font("Arial", FontWeight.NORMAL, 10));
				}else {
					((Text)VCentre.getChildren().get(i)).setFont(Font.font("Arial", FontWeight.BOLD, 14));
					
				}
			}
		}
		
		/**
		 * Boucle permettant de faire la mise en forme de la partie droite.
		 */
		for (int i = 0; i < VDroite.getChildren().size(); i++) {
			
			if (VDroite.getChildren().get(i).getClass().equals(Text.class)) {
				
				if( ((Text)VDroite.getChildren().get(i)).getText().equals("\n")) {
					
					((Text)VDroite.getChildren().get(i)).setFont(Font.font("Arial", FontWeight.NORMAL, 10));
				}
			}
			if (VDroite.getChildren().get(i).getClass().equals(Label.class)) {
				((Labeled) VDroite.getChildren().get(i)).setFont(Font.font("Arial", FontWeight.NORMAL, 13));
			}
			if (VDroite.getChildren().get(i).getClass().equals(RadioButton.class)) {
				
				((RadioButton) VDroite.getChildren().get(i)).setFont(Font.font("Arial", FontWeight.NORMAL, 13));
			}	
		}
	}
	/**
	 * Methode permettant de definir le box de la liste de clients.
	 * @param pVBox
	 * Le parametre est un verticalBox.
	 */
	public void setListeDesClients(VBox pVBox) {
		
		listeDesClientText = new Text("\n\tListe des clients :\n");
		listeDesClientText.setFont(Font.font("Arial", FontWeight.BOLD, 14));
			
		labelClientList = new Label("");
		
		//listeDesClientText.setFont(Font.font("Arial", FontWeight.BOLD, 13));
		
		pVBox.getChildren().addAll(listeDesClientText, labelClientList);
	}
	
	/**
	 * Methode permettant de definir le box de la modification des clients.
	 * @param pVBox1
	 * Le parametre est un verticalBox.
	 */
	public void setModificationDunClient (VBox pVBox1) {
		
		modificationDunClientText = new Text("\n\tModification d'un client\n");
		modificationDunClientText.setFont(Font.font("Arial", FontWeight.BOLD, 14));
		//modificationDunClientText.setFont(Font.font("Arial", FontWeight.BOLD, 13));
		numeroDeClientLabel = new Label ("Numero de client :");
		numeroDuClientTextField = new TextField();
		affichierUnClientButton = new Button("Afficher Client");
		
		nomLabel1 = new Label ("Nom :"); 
		nomTextField1 = new TextField();
		
		prenomLabel1 = new Label ("Prenom :");
		prenomTextField1 = new TextField();
		
		sexeLabel1 = new Label ("Sexe :");
		hommeButtonRadio1 = new RadioButton("Homme\t\t");
	
		//hommeButtonRadio1.setFont(Font.font("Arial", FontWeight.NORMAL, 12));
		femmeButtonRadio1 = new RadioButton("Femme");
		//femmeButtonRadio1.setFont(Font.font("Arial", FontWeight.NORMAL, 12));
		HBox hori0 = new HBox();
		hori0.getChildren().addAll(hommeButtonRadio1, femmeButtonRadio1);
		sexeGroup1 = new ToggleGroup();
		hommeButtonRadio1.setToggleGroup(sexeGroup1);
		femmeButtonRadio1.setToggleGroup(sexeGroup1);
		
		ageLabel1 = new Label ("Age :");
		ageTextField1 = new TextField();
		
		statutLabel1 = new Label ("Statut :");
		envigueurButtonRadio = new RadioButton("En vigueur");
		suspenduButtonRadio = new RadioButton("Suspendu");
		HBox hori2 = new HBox();
		hori2.getChildren().addAll(envigueurButtonRadio, new Text("\t\t"), suspenduButtonRadio);
		statutGroup = new ToggleGroup();
		envigueurButtonRadio.setToggleGroup(statutGroup);
		suspenduButtonRadio.setToggleGroup(statutGroup);
		
		modifierClientButton = new Button("Modifier Client");
	
		pVBox1.getChildren().addAll(modificationDunClientText, 
				numeroDeClientLabel, 
				numeroDuClientTextField,affichierUnClientButton, 
				new Text("\n"),
				nomLabel1, nomTextField1, 
				new Text("\n"), 
				prenomLabel1, prenomTextField1, 
				new Text("\n"), 
				sexeLabel1, hori0, 
				new Text("\n"), 
				ageLabel1, ageTextField1, 
				new Text("\n"), 
				statutLabel1, hori2, 
				new Text("\n"),
				modifierClientButton);
		
	}
	
	/**
	 * Methode permettant de definir ke box de l'ajout des clients.
	 * @param pVBox2
	 * Le parametre est un verticalBox.
	 */
	public void setAjoutDunClient(VBox pVBox2) {
		
		ajoutDunClientText = new Text ("\n\tAjout d'un client\n");
		ajoutDunClientText.setFont(Font.font("Arial", FontWeight.BOLD, 14));
		nomLabel2 = new Label ("Nom :"); 
		prenomLabel2 = new Label ("Prenom :");
		sexeLabel2 = new Label ("Sexe :");
		ageLabel2 = new Label ("Age :");
		
		nomTextField2 = new TextField();
		prenomTextField2 = new TextField();
		ageTextField2 = new TextField();
		
		hommeButtonRadio2 = new RadioButton("Homme");
		femmeButtonRadio2 = new RadioButton("Femme");
		HBox hori3 = new HBox();
		hori3.getChildren().addAll(hommeButtonRadio2, new Text("\t\t"), femmeButtonRadio2);
		sexeGroup2 = new ToggleGroup();
		hommeButtonRadio2.setToggleGroup(sexeGroup2);
		femmeButtonRadio2.setToggleGroup(sexeGroup2);
		ajouterClientButton = new Button ("Ajouter Client");
		
		pVBox2.getChildren().addAll(ajoutDunClientText, nomLabel2, nomTextField2, 
				new Text("\n"),
				prenomLabel2, prenomTextField2,
				new Text("\n"),
				sexeLabel2, hori3, 
				new Text("\n"),
				ageLabel2, ageTextField2, 
				new Text("\n"),
				ajouterClientButton);
		 
	}

	/**
	 * @return le root de la vue.
	 */
	public BorderPane getPaneGestionClient() {
		return bPaneGestionClient;
	}
	
	/**
	 * Definit le root de la vue.
	 * @param bPaneGestionClient
	 * Le root de la vue
	 */
	public void setbPaneGestionClient(BorderPane bPaneGestionClient) {
		this.bPaneGestionClient = bPaneGestionClient;
	}

	/**
	 * 
	 * @return le label contenant la liste des clients.
	 */
	public Label getLabelClientList() {
		return labelClientList;
	}

	/**
	 * @return le textFiel numero client.
	 */
	public TextField getNumeroDuClientTextField() {
		return numeroDuClientTextField;
	}


	/**
	 * @return le textField nom client pour la modification.
	 */
	public TextField getNomTextField1() {
		return nomTextField1;
	}

	/**
	 * @return le textField prenom du client pour la modification.
	 */
	public TextField getPrenomTextField1() {
		return prenomTextField1;
	}

	/**
	 * @return le textField sexe du client pour la modification.
	 */
	public TextField getSexeTextField1() {
		return sexeTextField1;
	}

	/**
	 * @return le textField age du client pour la modification.
	 */
	public TextField getAgeTextField1() {
		return ageTextField1;
	}

	/**
	 * @return le textField nom du client pour l'ajout.
	 */
	public TextField getNomTextField2() {
		return nomTextField2;
	}

	/**
	 * @return le textField prenom du client pour l'ajout.
	 */
	public TextField getPrenomTextField2() {
		return prenomTextField2;
	}

	/**
	 * @return le textField age du client pour l'ajout.
	 */
	public TextField getAgeTextField2() {
		return ageTextField2;
	}
	
	/**
	 * @return le bouton radio de genre masculin pour la modification.
	 */
	public RadioButton getHommeButtonRadio1() {
		return hommeButtonRadio1;
	}
	
	/**
	 * @return le bouton radio de genre masculin pour l'ajout.
	 */
	public RadioButton getHommeButtonRadio2() {
		return hommeButtonRadio2;
	}

	/**
	 * @return le bouton radio de genre feminin pour la modification.
	 */
	public RadioButton getFemmeButtonRadio1() {
		return femmeButtonRadio1;
	}

	/**
	 * @return le bouton radio de genre feminin pour l'ajout.
	 */
	public RadioButton getFemmeButtonRadio2() {
		return femmeButtonRadio2;
	}

	/**
	 * @return le bouton radio du staut "En vigueur" pour la modification.
	 */
	public RadioButton getEnvigueurButtonRadio() {
		return envigueurButtonRadio;
	}

	/**
	 * @return le bouton radio du staut "Suspendur" pour l'ajout.
	 */
	public RadioButton getSuspenduButtonRadio() {
		return suspenduButtonRadio;
	}

	/**
	 * @return le bouton afficher client.
	 */
	public Button getAffichierUnClientButton() {
		return affichierUnClientButton;
	}

	
	/**
	 * @return le bouton modifier client.
	 */
	public Button getModifierClientButton() {
		return modifierClientButton;
	}

	/**
	 * @return le bouton ajouter client.
	 */
	public Button getAjouterClientButton() {
		return ajouterClientButton;
	}

	/**
	 * @return le groupe de bouton radio sexe pour la modification.
	 */
	public ToggleGroup getSexeGroup1() {
		return sexeGroup1;
	}

	/**
	 * @return le groupe de bouton radio sexe pour l'ajout.
	 */
	public ToggleGroup getSexeGroup2() {
		return sexeGroup2;
	}

	/**
	 * @return le groupe de bouton radio statut du client pour la modification.
	 */
	public ToggleGroup getStatutGroup() {
		return statutGroup;
	}


	

	
	

	
	
	
}