package enums;

/**
 * Cette classe enumeration permet d'avoir les differents statuts de films.
 * @author akimsoule
 * @version 1.0.
 *
 */
public enum StatusFilm {
	DISPONIBLE, LOUE
}
