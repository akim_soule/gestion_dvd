package app;

import enums.GenreFilm;
import enums.StatusFilm;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import produit.Produit;

/**
 * Cette classe correspond a la vueGestionLocations.
 * Elle contient la liste des films et un formulaire permettant la gestion ds locations;
 * notamment permettre la location d'un film ou le retour d'un film.
 * @author akimsoule.
 * @version 1.0.
 *
 */
public class VueGestionLocations {

	/**
	 * le root de la vue
	 */
	private BorderPane bPaneGestionLocation = new BorderPane();
	
	/**
	 * la table film pour lister les films
	 */
	private TableView<Produit> tableFilm = new TableView<Produit>();
	
	/**
	 * le combo film pour permettre la selection d'un film
	 */
	private ComboBox<Produit> cboFilm;
	
	/**
	 * les boutons louer film et retourner un film
	 */
	private Button btnCreerLocation;
	private Button btnRetourLocation;
	
	/**
	 * le label location d'un film
	 */
	private Label lblListeGestionLocationDVD;
	
	/**
	 * les textfields pour recuperer les informations
	 */
	private TextField txtNoFilm, txtNoClient;
	private TextField inputNoClientGestionLocation;
	
	/**
	 * le scrollpane qui permet d'afficher le contexte de location;
	 * le tableau statique a 2 dimensions est affiche dans ce scrollpane.
	 */
	private ScrollPane s1;
	
	
	/**
	 * Constructeur principal de la classe permettant de mettre en place
	 * le box de l'inventaire des films et celui du gestionnaire des locations.
	 */
	public VueGestionLocations() {
		VBox vBoxInventaireFilms = new VBox();
		setInventairesFilms(vBoxInventaireFilms);

		VBox vBoxGestionnaireLocations = new VBox();
		setGestionnaireLocations(vBoxGestionnaireLocations);

		bPaneGestionLocation.setLeft(vBoxInventaireFilms);
		bPaneGestionLocation.setRight(vBoxGestionnaireLocations);
		bPaneGestionLocation.setPadding(new Insets(4));
	}

	/**
	 * @return le root de la vue.
	 */
	public BorderPane getPaneGestionLocation() {
		return bPaneGestionLocation;
	}

	/**¸
	 * Permet de definir le box de l'invntaire des films.
	 * @param filmVBox
	 * Le parametre utilise est un vertical box.
	 */
	private void setInventairesFilms(VBox filmVBox) {
		tableFilm.setEditable(false);
		filmVBox.setMinSize(475, 500);

		Text titre = new Text("\n\tInventaire des films :\n");
		titre.setFont(Font.font("Arial", FontWeight.BOLD, 14));

		TableColumn col1 = new TableColumn("No");
		TableColumn col2 = new TableColumn("Nom");
		TableColumn col3 = new TableColumn("Genre");
		TableColumn col4 = new TableColumn("Loué");

		tableFilm.getColumns().addAll(col1, col2, col3, col4);

		col1.setCellValueFactory(new PropertyValueFactory<Produit, Integer>("id"));
		col2.setCellValueFactory(new PropertyValueFactory<Produit, String>("nom"));
		col3.setCellValueFactory(new PropertyValueFactory<Produit, GenreFilm>("genre"));
		col4.setCellValueFactory(new PropertyValueFactory<Produit, StatusFilm>("status"));

		filmVBox.getChildren().addAll(titre, tableFilm);
	}

	/**
	 * Permet de definir le box des gestionnaire des locations.
	 * @param vBoxGestionLocations
	 * Le parametre est un vertical box.
	 */
	private void setGestionnaireLocations(VBox vBoxGestionLocations) {
		vBoxGestionLocations.setMaxSize(475, 500);
		vBoxGestionLocations.setPrefSize(475, 500);
		// reservationVBox.setPadding(new Insets(10));

		Label lblGestionLocations = new Label("\n\tGestionnaire des locations");
		lblGestionLocations.setFont(Font.font("Arial", FontWeight.BOLD, 14));

		Label lblLouer = new Label("\n1) Location d'un film :");
		lblLouer.setFont(Font.font("Arial", FontWeight.BOLD, 13));

		Label lblChoisirClient = new Label("\nChoisir un client :");
		lblChoisirClient.setFont(Font.font("Arial", FontWeight.BOLD, 12));

		Label lblChoisirFilm = new Label("\nChoisir un film :");
		lblChoisirFilm.setFont(Font.font("Arial", FontWeight.BOLD, 12));

		Label lblTableLocations = new Label("\nVoici la table des locations : ");

		Label lblRetour = new Label("2) Retour d'un film :");
		lblRetour.setFont(Font.font("Arial", FontWeight.BOLD, 13));

		Label lbltitreRetourFilm = new Label("\nEntrez les infos de retour : ");
		lbltitreRetourFilm.setFont(Font.font("Arial", FontWeight.BOLD, 12));

		Label espace1 = new Label("\t");
		Label espace3 = new Label("\n");

		Label lblEntrezNoFilm = new Label("#Film : ");
		Label lblEntrezNoClient = new Label("#Client : ");

		inputNoClientGestionLocation = new TextField();
		cboFilm = new ComboBox<Produit>();
		btnCreerLocation = new Button("Louer un film");
		btnRetourLocation = new Button("Retourner un film");
		lblListeGestionLocationDVD = new Label();

		s1 = new ScrollPane();
		s1.setContent(lblListeGestionLocationDVD);
		s1.setHbarPolicy(ScrollBarPolicy.ALWAYS);
		s1.setVbarPolicy(ScrollBarPolicy.ALWAYS);

		txtNoFilm = new TextField();
		txtNoClient = new TextField();

		HBox hBoxBoutonsRetourFilm = new HBox();
		hBoxBoutonsRetourFilm.getChildren().addAll(lblEntrezNoClient, txtNoClient, espace1, lblEntrezNoFilm,
				txtNoFilm);

		vBoxGestionLocations.getChildren().addAll(lblGestionLocations, lblLouer, lblChoisirClient,
				inputNoClientGestionLocation, lblChoisirFilm, cboFilm, btnCreerLocation, lblTableLocations, s1, espace3,
				lblRetour, lbltitreRetourFilm, hBoxBoutonsRetourFilm, btnRetourLocation);
	}
	
	/**
	 * @return le film selectionne dans la tabFilm.
	 */
	public Produit getFilmCboSelectionne(){
		return cboFilm.getSelectionModel().getSelectedItem();
	}

	/**
	 * @return le bouton louer un film.
	 */
	public Button getBoutonCreerLocation() {
		return btnCreerLocation;
	}
	
	/**
	 * @return le bouton retourner un film.
	 */
	public Button getBoutonRetournerLocation() {
		return btnRetourLocation;
	}
	
	/**
	 * Permet de mettre du texte dans du label location d'un film.
	 * @param listeGestionLocationDVD
	 * Le parametre est une chaine de caracteres
	 */
	public void setListeGestionLocationDVD(String listeGestionLocationDVD) {
		lblListeGestionLocationDVD.setText(listeGestionLocationDVD);
	}

	/**
	 * Permet de mettre des films dans le comboBox cboFilm.
	 * @param listeFilm
	 * Le parametre utilise est une liste observable de produits.
	 */
	public void setComboBoxFilm(ObservableList<Produit> listeFilm) {
		cboFilm.setItems(listeFilm);
	}
	
	/**
	 * Permet de mettre des flms dans la tableFilm.
	 * @param observableList
	 * Le parametre utilise est une liste observable.
	 */
	public void setTableViewFilm(ObservableList<Produit> observableList) {
		tableFilm.setItems(observableList);
	}
	
	/**
	 * Pemet de rafraichir la table des films.
	 */
	public void refreshTableViewFilms() {
		tableFilm.refresh();
	}
	
	/**
	 * Retourne la chaine de caracteres entres dans le textField numero de film.
	 * @return une chaine de caracteres.
	 */
	public String getTxtNoFilm() {
		return txtNoFilm.getText();
	}
	
	/**
	 * Permet de mettre du texte dans la table des films.
	 * @param texte
	 * Le parametre utilise est une chaine de caracteres.
	 */
	public void setTxtNoFilm(String texte){
		txtNoFilm.setText(texte);
	}
	
	/**
	 * Retourne la chaine de caracteres entres dans le textField numero de client.
	 * @return une chaine de caracteres.
	 */
	public String getTxtNoClient() {
		return txtNoClient.getText();
	}
	
	/**
	 * Permet de mettre du texte dans le textField numero film.
	 * @param texte
	 * Le parametre utilise est une chaine de caracteres.
	 */
	public void setTxtNoClient(String texte){
		txtNoClient.setText(texte);
	}
	
	/**
	 * Retourne la chaine de caracteres entres dans le textField pour choisir le client.
	 * @return une chaine de caracteres.
	 */
	public String getInputNoClientGestionLocation() {
		return inputNoClientGestionLocation.getText();
	}

	
}