package enums;


/**
 * Cette classe enumeration permet d'avoir les differents status du client.
 * @author akimsoule
 * @version 1.0.
 *
 */
public enum StatusClient {
	CREATION, EN_VIGUEUR, SUSPENDU
}
