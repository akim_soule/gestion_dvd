package produit;

import enums.GenreFilm;
import enums.StatusFilm;

/**
 * Cette classe represente un film Comedie. Elle herite de la classe Film.
 * Elle a pour principal attribut l'age minimum qui est de 6 ans.
 * @author akimsoule
 * @version 1.0
 *
 */
public class Comedie extends Film {

	/**
	 * Attribut static de la classe; l'age minimum requis pour regarder un film de comedie.
	 */
	public static final int AGE_MIN = 6;
	
	/**
	 * Premier constructeur de la classe utilisant l'id et le nom du film.
	 * @param pId
	 * l'id du film.
	 * @param nom
	 * le nom du film.
	 */
	public Comedie(int pId, String nom) {
		super(pId, nom);
	}
	
	/**
	 * Deuxieme constructeur de la classe utilisant l'id, le nom et le status du film.
	 * @param pId
	 * l'id du film.
	 * @param pnom
	 * le nom du film.
	 * @param pstatusFilm
	 * le statut du film.
	 */
	public Comedie(int pId, String pnom, StatusFilm pstatusFilm) {
		super(pId, pnom, pstatusFilm);
	}
	
	/**
	 * @return le genre du film.
	 */
	public GenreFilm getGenre() {
		// TODO Auto-generated method stub
		return GenreFilm.COMEDIE;
	}

	/**
	 * @return l'age minimum requis pour regarder le film.
	 */
	public int getAgeMin() {
		// TODO Auto-generated method stub
		return 6;
	}
	

	

}
