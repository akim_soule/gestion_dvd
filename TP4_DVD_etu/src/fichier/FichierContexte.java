package fichier;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.plaf.synth.SynthSpinnerUI;

/**
 * Cette classe permet d'ecrire dans le fichier Contexte.dat et d'afficher son contenu.
 * @author akimsoule
 * @version 1.0.
 *
 */
public class FichierContexte {
	
	/**
	 * Permet d'ecrire une chaine de caracteres dans le fichier Contexte.dat.
	 * @param file
	 * Le parametre file est le fichier Contexte.dat.
	 * @param donne
	 * La donnee est une chaine de caracteres.
	 */
	public static void ecrireFichierContexte(File file, String donne) {
		
		//System.out.println("la methode ecrireFichierContexte\n"+donne);
		try {
			BufferedOutputStream  bos = new BufferedOutputStream(new FileOutputStream(file));
			byte[] b = donne.getBytes();
			
			bos.write(b);
			
			bos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 *  Permet de lire le contenu du fichier Contexte.dat et de retourner un tableau statique d'entiers.
	 * @param file
	 * Le file est le fichier Contexte.dat.
	 * @return le tableau statique contenu dans le fichier.
	 */
	public static int[][] lireFichierContexte (File file) {
		String ligneLu = "";
		
		int tab [][] = new int [Constantes.MAX_CLIENT][Constantes.MAX_FILM + 2];

		try {
			FileReader fluxSource2 = new FileReader(file);
			BufferedReader bufferSource2 = new BufferedReader(fluxSource2);
			ligneLu = bufferSource2.readLine();
			int i = 0;
			while (ligneLu != null) {
				String parts[] = ligneLu.split(";");
				for(int j = 0; j < parts.length; j++) {
					tab[i][j] = Integer.parseInt(parts[j]);
				}
				
				
				ligneLu = bufferSource2.readLine();
				i++;
			}			
	
			bufferSource2.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tab;
	}
	
	
	public static void afficherTab(int tab[][]) {
		
		for(int i = 0; i < tab.length; i++) {
			for (int j = 0; j < tab[i].length; j++) {
				if(tab[i][j] != 0) {
					System.out.print("|"+tab[i][j]+"|");
				}
				
			}
			System.out.println();
		}
	}
	
//	public static void main(String[] args) {
//		File file = new File ("res/fichiers/Contexte.dat");
//		afficherTab(lireFichierContexte(file));
//				
//		//lireFichierContexte(file);
//	}
}
