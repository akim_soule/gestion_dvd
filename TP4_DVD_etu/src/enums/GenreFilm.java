package enums;

/**
 * Cette classe enumeration permet d'avoir les differents genres de films.
 * @author akimsoule
 * @version 1.0.
 *
 */
public enum GenreFilm {
	ACTION, COMEDIE, HORREUR, SCIENCE_FICTION
}
