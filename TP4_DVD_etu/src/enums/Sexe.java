package enums;

/**
 * Cette classe enumeration permet d'avoir les differents genres humains.
 * @author akimsoule
 * @version 1.0.
 *
 */
public enum Sexe {
	FEMININ, MASCULIN
};
