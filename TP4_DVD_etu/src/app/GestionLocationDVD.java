package app;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;

import enums.Sexe;
import enums.StatusClient;
import enums.StatusFilm;
import exception.lanceralerte;
import fichier.Constantes;
import fichier.FichierContexte;
import fichier.FichierFilms;

import java.io.FileOutputStream;
import humain.Client;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import produit.Action;
import produit.Film;
import produit.Produit;

/**
 * Cette classe permet de gerer la location. Elle a pour principal attibut
 * la liste des clients, la liste des films, et le tableau de gesitonLocation.
 * Ce dernier permet de stocker la liste des clients ainsi que leur location.
 * @author akimsoule
 *@version 1.0
 */
public class GestionLocationDVD {

	/**
	 * La liste des clients
	 */
	private ArrayList<Client> listeClient;
	
	/**
	 * La liste des films
	 */
	private ObservableList<Produit> listeFilm;
	
	/**
	 * La tableau gestionLocation qui stocke les clients enregistres ainsi que les locations avec des valeurs.
	 */
	private int[][] gestionLocation;

	/**
	 * Principal constructeur de gestionLocationDVD qui instancie les attributs de la classe.
	 */
	public GestionLocationDVD() {
		listeClient = new ArrayList<Client>();
		listeFilm = FXCollections.observableArrayList();
				
		gestionLocation = new int[Constantes.MAX_CLIENT][Constantes.MAX_FILM + 2];
		// rediriger les messages d'erreur vers le fichier error.txt
		
		try {
			System.setErr(new PrintStream(new FileOutputStream(new File("./error.txt"))));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Cette methode permet de retourner la liste des films.
	 * @return la liste des films.
	 */
	public ObservableList<Produit> getObervableListeFilm() {
		return listeFilm;
	}
	
	
	/**
	 * Cette methode permet le lire la liste des films et de l'attribuer dans la liste des films.
	 * @param file
	 * Le parametre est appele dans la classe Controler.
	 */
	public void lireFichierFilms(File file) {
		
		this.listeFilm = FichierFilms.lireFichierFilm(file);
		
	}
	
	/**
	 * Cette methode de lire le fichier Client.dat et des les ajouter la liste des clients.
	 */
	public void lireFichierClients() {
		
	ArrayList<Client> AC = new ArrayList<Client>();
	AC = FichierFilms.lireFichierClient();
			
		for(int i = 0; i < AC.size(); i++) {
			if(!this.listeClient.contains(AC.get(i)))
			this.listeClient.add(i, AC.get(i));
			
		}
		
	}
	
	/**
	 * Cette methode permet d'ecrire la liste des clients dans le fichier Clients.dat.
	 * @param clients
	 * La liste des clients entree en parametre
	 */
	public void ecrireFichierClients(ArrayList<Client> clients) {
		
		clients = this.listeClient;
		
		//System.out.println(clients.toString());
		FichierFilms.ecrireFichierClient(clients);
		
	}
	
	/**public void lireFichierFilms(String nomFichier) {
		
		GestionLocationDVD ob = new GestionLocationDVD();
		File fichierFilmsTxt = new File(ob.getClass().getResource(nomFichier).getFile());
		
		
		
		this.listeFilm = FichierFilms.lireFichierFilm(fichierFilmsTxt);
		
	}**/
	
	/**
	 *Retourne le contexte de location dans le scrollPane de la vue gestion locations.
	 * @return une chaine de caracteres qui decrit de le contexte de location.
	 */
	public String getListeGestionLocationDVD() {
		
		String retour = "";
		
		for (int i = 0; i < listeClient.size(); i++) {

			if (2 >= gestionLocation[i][1] + 2)
				retour += "Client " + (i+1) + " : aucune location";
			else
				retour += "Client " + (i+1) + " : ";

			for (int j = 2; j < gestionLocation[i][1] + 2; j++)
				retour += " Location " + (j - 1) + " : #" + gestionLocation[i][j] + "    ";

			retour += "\n";
		}
		return retour;
	}
	
	/**
	 * Ajouter un client à la liste des clients
	 * 
	 * @param client Le client ne doit pas exister dans le liste.
	 * 
	 */
	 public void ajouterClient(Client client) {
		 
		 if (!this.listeClient.contains(client)) {
			
			 this.listeClient.add(client);
			 
			 //gestionLocation = new int[Constantes.MAX_CLIENT][Constantes.MAX_FILM + 2];
		 
			 gestionLocation[client.getNumero()][0] = client.getNumero();
			
			 //System.out.println("Operation effectuee avec succes");
		 }
		
		 
		// System.out.println(listeClient.toString()); 
	}	 
	 
		
	 
	/**
	 * Retourne le contexte le location, celui contenu dans le tableau gestionLocation.
	 * @return une chaine de caracteres.
	 */
	public String getContexte() {
		String retour = "";
		for (int i = 0; i < listeClient.size(); i++) {

			retour += (i + 1) + ";" + gestionLocation[i][1] + ";";

			for (int j = 2; j < gestionLocation[i][1] + 2; j++)
				retour += gestionLocation[i][j] + ";";

			retour += "\n";
		
		
		}
		System.out.println(retour);
		return retour;
	}
	
	/**
	 * @return la liste des clients.
	 */
	public ArrayList<Client> getListeClient() {
		return this.listeClient;
	}
	
	/**
	 * Retourne vrai ou faux selon si la location est possible ou pas.
	 * @param pClient
	 * Les parametres du client sont verifies pour savoir si la location peut lui etre accordee.
	 * @param pFilm
	 * Le film doit etre dispionible.
	 * @return vrai ou faux
	 */
	public boolean louerFilm(Client pClient, Film pFilm) {
		boolean ok = false;
		String message = "";
		
		if(pFilm.getStatus().equals(StatusFilm.LOUE) || 
				pClient.getStatus().equals(StatusClient.SUSPENDU) ||
					pClient.getAge() < pFilm.getAgeMin()) {
			
			ok = false;
		}else { 
			ok = true;
			
		}
		
		if (ok == true) {
			for(int i = 0; i < listeFilm.size(); i++) {
				if(pFilm.getId() == listeFilm.get(i).getId()) {
					listeFilm.get(i).setStatus(StatusFilm.LOUE);
				}
			}
			//Pour le nombre de films loues
			gestionLocation[pClient.getNumero() -1][1] = gestionLocation[pClient.getNumero() -1][1] + 1;
			//Pour l'id du film loue
			gestionLocation[pClient.getNumero() -1][  gestionLocation[pClient.getNumero() -1][1]   + 1] = pFilm.getId();
		}else {
			
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Location film");
			alert.setHeaderText("Location film impossible");
			if (pFilm.getStatus().equals(StatusFilm.LOUE)){
				message += "Le film est deja loue\n";
			}
			if (pClient.getStatus().equals(StatusClient.SUSPENDU)){
				message += "Le client est suspendu\n";
			}
			if(pClient.getAge() < pFilm.getAgeMin()) {
				message += "Le client n'a pas l'age requis\nIl a "+pClient.getAge()+" ans";
			}
			alert.setContentText(message);
			alert.showAndWait();
		}
		
		return ok;
	}
	
	/**
	 * Cette methode permet d'ecrire le contexte de location dans le fichier Contexte.dat.
	 * Elle est appelee dans la classe Controler.
	 * @param donne 
	 * La donne est appelee avec la methode getContexte() dans la classe Controler.
	 */
	public void ecrireFichierContexte(String donne) {
		
		
		
		GestionLocationDVD obj = new GestionLocationDVD();
		
		File file = new File(obj.getClass().getResource(Constantes.NOM_FICHIER_CONTEXTE).getFile());
		
		FichierContexte.ecrireFichierContexte(file, donne);
		
	}
	
	/**
	 * Cette methode permet de retourner un film. Elle reajuste le tableau de gestionLocation en consequence.
	 * @param nbFilm
	 * Le film qui doit etre retourne
	 * @param nbClient
	 * Le client qui retourne le client.
	 */
	public void retourFilm(int nbFilm, int nbClient) {
		
		ArrayList <Integer> listInt = new ArrayList<Integer>();
		
		boolean trouveFilm = false;
		//boucle de recherche de film
		for (int i = 0; i < gestionLocation[nbClient-1].length; i++) {
			if (i >= 2) {
				if (gestionLocation[nbClient-1][i] == nbFilm) {
					System.out.println(gestionLocation[nbClient-1][i]);
					trouveFilm = true;
				}
			}
		}
		//si le film est trouve
		if(trouveFilm) {
			if (gestionLocation[nbClient-1][1] > 0) {
				gestionLocation[nbClient-1][1] = gestionLocation[nbClient-1][1] -1;
				int position = 0;
				for(int j = 0; j < this.gestionLocation[0].length; j++) {
					if(j >=2 && this.gestionLocation[nbClient-1][j] == nbFilm) {
						position = j;
						if(!listInt.contains(this.gestionLocation[nbClient-1][j])) {
							listInt.add(this.gestionLocation[nbClient-1][j]);
							
						}
						while(this.gestionLocation[nbClient-1][position] != 0) {
							gestionLocation[nbClient-1][position] = gestionLocation[nbClient-1][position + 1];
							position = position + 1;
							}
							gestionLocation[nbClient-1][position] = 0;
						j = this.gestionLocation[0].length;
					}
				}
			}
		}else {
			new lanceralerte("Aucun film trouve", 
					"Le client "+nbClient+" n'a aucune location correspondant au film "+nbFilm);
		}
		
		/**
		 * bouble qui permet de rendre le film loue disponible.
		 */
		for (int k = 0; k < listInt.size(); k++) {
			for (int i = 0; i < listeFilm.size(); i++) {
				if (listeFilm.get(i).getId() == listInt.get(k)) {
					
					listeFilm.get(i).setStatus(StatusFilm.DISPONIBLE);
					System.out.println(listeFilm.get(i));
					
				}
			}
		}
		
	}
	
	/**
	 * Methode qui permet d'afficher la liste des clients en chaine de caracteres.
	 * @return une chaine de caracteres.
	 */
	public String afficherClientList() {
		String retour = "";
		for (int i = 0; i < listeClient.size(); i++) {
			retour+=listeClient.get(i).toString()+"\n";
		}
		
		return retour;
	}

	/**
	 * Methode qui permet de lire un fichier Contexte.dat.
	 * @param file
	 * Le parametre file Contexte.dat est appele dans la classe Controler.
	 */
	public void lireFichierContexte(File file) {
		this.gestionLocation = FichierContexte.lireFichierContexte(file);
		
		
		ArrayList <Integer> listInt = new ArrayList<Integer>();
		//boucle pour ajouter des numero dans l'arraylist
		for(int i = 0; i < this.gestionLocation.length; i++) {
			for(int j = 0; j < this.gestionLocation[i].length; j++) {
				if(j >=2 && this.gestionLocation[i][j] != 0) {
					if(!listInt.contains(this.gestionLocation[i][j])) {
						listInt.add(this.gestionLocation[i][j]);
					}	
				}
			}
		}
		
		//this.listeFilm = FichierFilms.lireFichierFilm(file);
		
		/**
		 * Le film devient louer.
		 */
		for (int k = 0; k < listInt.size(); k++) {
			for (int i = 0; i < listeFilm.size(); i++) {
				if (listeFilm.get(i).getId() == listInt.get(k)) {
					listeFilm.get(i).louer();
					
				}
				
			}
		}
		
		
	}
	 
}
