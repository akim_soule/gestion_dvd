package app;


import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import produit.Film;

import java.io.File;
import java.util.ArrayList;

import javax.swing.event.EventListenerList;

import enums.Sexe;
import enums.StatusClient;
import exception.lanceralerte;
import fichier.Constantes;
import fichier.FichierContexte;
import fichier.FichierFilms;
import humain.Client;
import humain.Personne;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;

/**
 * La classe controler comprend les methodes principales qui gerent le coeur de l'application.
 * Elle permet d'ajouter un client, de le modifier, de louer un film a un client, et de permettre 
 * le retour d'un film.
 * 
 * Elle a pour principals attibuts la vue principale, la vue de gestionClients, celle de gestionLocations, 
 * un objet de la classe GestionLocationDVD qui gere la liste des clients et la liste des films.
 * 
 * 
 * Elle a pour parametre important : gestionLocDVD qui est le coeur du programme.
 * 
 * @author akimsoule
 * @version 1.0
 *
 */
public class Controller extends Application {

	/**
	 * La vue principale qui contient les deux onglets de l'application que sont :
	 * vueGestionCliens et vueGestionLocations.
	 */
	private VuePrincipale vuePrincipale;
	
	/**
	 * La vue de GestionClients.
	 */
	private VueGestionClients vueGestionClients;
	
	/**
	 * La vue de GestionLocations
	 */
	private VueGestionLocations vueGestionLocations;

	/**
	 * L'objet de la classe GestionLocationDVD qui gere la liste des clients et la liste des films.
	 */
	private GestionLocationDVD gestionLocDVD;
	
	/**
	 * Le stage qui permet a'afficher la fenetre de l'application.
	 */
	private Stage primaryStage;

	/**
	 * La methode start qui permet de demarrer l'application.
	 */
	public void start(Stage stage) throws Exception {
		primaryStage = stage;
		primaryStage.setTitle(" Location DVD");
		primaryStage.getIcons().add(new Image(Constantes.NOM_ICONE));
		primaryStage.setResizable(false);

		gestionLocDVD = new GestionLocationDVD();
		vueGestionClients = new VueGestionClients();
		
		vueGestionLocations = new VueGestionLocations();
		
		gestionLocDVD.lireFichierFilms(new File(getClass().getResource(Constantes.NOM_FICHIER_FILM).getFile()));
		
		vueGestionLocations.setTableViewFilm(gestionLocDVD.getObervableListeFilm());
		vueGestionLocations.setComboBoxFilm(gestionLocDVD.getObervableListeFilm());
		vueGestionLocations.setListeGestionLocationDVD(gestionLocDVD.getListeGestionLocationDVD());
		
		vuePrincipale = new VuePrincipale(vueGestionClients, vueGestionLocations);

		primaryStage.setScene(vuePrincipale.getMainScene());
		primaryStage.show();
		primaryStage.centerOnScreen();
		
		
		
		gestionLocDVD.lireFichierClients();
		 
		vueGestionClients.getLabelClientList().setText(gestionLocDVD.afficherClientList()); 
		connecteEcouteurs();
		
	}

	/**
	 * Cette methode permet d'ajouter des ecouteurs aux differents elements interactifs de la fenetre.
	 */
	private void connecteEcouteurs() {

			
		vueGestionClients.getModifierClientButton().setOnAction(new GestionnaireBouton());
		vueGestionClients.getAffichierUnClientButton().setOnAction(new GestionnaireBouton());
		vueGestionClients.getAjouterClientButton().setOnAction(new GestionnaireBouton());
		
		vueGestionLocations.getBoutonCreerLocation().setOnAction(new GestionnaireBouton());
		vueGestionLocations.getBoutonRetournerLocation().setOnAction(new GestionnaireBouton());
		
		vuePrincipale.getBoutonLireContexte().setOnAction(new GestionnaireBouton());
		vuePrincipale.getBoutonEcrireContexte().setOnAction(new GestionnaireBouton());
		vuePrincipale.getBoutonQuitter().setOnAction(new GestionnaireBouton());
	}

	/**
	 * La classe GestionnaireBouton permet d'attribuer une methode a chaque bouton de l'application.
	 * Pour louer, la methode est creerLocation(); le bouton retour film a pour correspondance retournerLocation().
	 * @author akimsoule.
	 * version 1.0.
	 *
	 */
	private class GestionnaireBouton implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			if (event.getSource() == vueGestionLocations.getBoutonCreerLocation())
				creerLocation();
			else if (event.getSource() == vueGestionLocations.getBoutonRetournerLocation())
				retournerLocation();
			else if (event.getSource() == vuePrincipale.getBoutonLireContexte())
				lireContexte();
			else if (event.getSource() == vuePrincipale.getBoutonEcrireContexte())
				ecrireContexte();
			else if (event.getSource() == vuePrincipale.getBoutonQuitter()) {
				Platform.exit();
			}else if (event.getSource() == vueGestionClients.getAjouterClientButton()) {
				ajouterClient();
			}else if (event.getSource() == vueGestionClients.getAffichierUnClientButton()) {
				afficherClient();
			}else if (event.getSource() == vueGestionClients.getModifierClientButton()) {
				modifierClient();
			}
		}
	}
	
//	private class ListViewHandler implements EventHandler<MouseEvent> {
//	  
//		ListView<Client> lView = vueGestionClients.getListeDesClientView();
//		
//	    public void handle(MouseEvent event) {
//	    	
//	    	int numeroClient = lView.getSelectionModel().getSelectedItem().getNumero();
//	    	
//	    	
//	    	//System.out.print(numeroClient);
//	    }
//	 }
	
	/**
	 * Cette permet d'ajouter un client a la liste des clients de l'objet gestionLocDVD.
	 * Elle ecrase et ecrit automatiquement la liste des clients de l'objet gestionLocDVD dans le fichier
	 * Client.dat.
	 * Par la suite, la liste des clients est actualisee pour l'affichage.
	 */
	private void ajouterClient() {
		String NomSaisi = vueGestionClients.getNomTextField2().getText();
		String PrenomSaisi = vueGestionClients.getPrenomTextField2().getText();
		
		Sexe sexeSelectionne = null;
		if(vueGestionClients.getHommeButtonRadio2().isSelected()) {
			 sexeSelectionne = Sexe.MASCULIN;
		}else if (vueGestionClients.getFemmeButtonRadio2().isSelected()) {
			sexeSelectionne = Sexe.FEMININ;
		}
		
		String message = "";
		boolean validation = false;
		if (!Personne.valideNomPrenom(NomSaisi)) {
			message += "Nom invalide\n";
			validation = false;
		}else {
			validation = true;
		}
		
		if (!Personne.valideNomPrenom(PrenomSaisi)) {
			message += "Prenom invalide\n";
			validation = false;
		}else {
			validation = true;
		}
		
		if (sexeSelectionne == null) {
			message += "Veuiller selectionner un sexe\n";
			validation = false;
		}else {
			validation = true;
		}
		
		int AgeSaisiString = 0;
		try {
			AgeSaisiString = Integer.parseInt(vueGestionClients.getAgeTextField2().getText());
			if (AgeSaisiString >= 6) {
				validation = true;
			}else {
				message += "L'age doit etre superieur a 6\n";
				validation = false;
			}
		}catch (Exception e){
			message += "L'age doit etre un entier\n";
			validation = false;
		}
		
		
		if (validation == false) {
			new lanceralerte("Ajout client incorrect",
						 message);
		}else {
			
			if(gestionLocDVD.getListeClient().size() > 0) {
				gestionLocDVD.getListeClient().get(gestionLocDVD.getListeClient().size()-1)
				.setNbClient(gestionLocDVD.getListeClient().get(gestionLocDVD.getListeClient().size()-1).getNumero());
			}
			
			
			Client nouveauClient = null;
			
			nouveauClient = new Client (NomSaisi, PrenomSaisi, sexeSelectionne, AgeSaisiString);
			
			gestionLocDVD.ajouterClient(nouveauClient);
			
			gestionLocDVD.ecrireFichierClients(gestionLocDVD.getListeClient());
			
			ObservableList<Client> oListClient = FXCollections.observableArrayList(gestionLocDVD.getListeClient());
			
			vueGestionClients.getLabelClientList().setText(gestionLocDVD.afficherClientList()); 
			
			
			
		}
	}

	/**
	 * Cette methode de creer une location. 
	 * Le client et le film sont selectionnes par l'utilisation de l'application.
	 * Si les conditions de locations sont reunies (age de client, film disponible , etc.), 
	 * le film est loue.
	 * La liste des films loues est actualise dans la liste des films.
	 */
	private void creerLocation() {
		
		int numeroEntre = 0;
		Client clientTrouve = null;
		boolean validation = false;
		Film filmSelectionne = (Film) vueGestionLocations.getFilmCboSelectionne();
		ObservableList<Client> list = FXCollections.observableArrayList(gestionLocDVD.getListeClient());
		
		try {
			
			numeroEntre = Integer.parseInt(vueGestionLocations.getInputNoClientGestionLocation());
			for (int i = 0; i < list.size(); i++) {
				
				if(numeroEntre == list.get(i).getNumero()) {
					
					clientTrouve = list.get(i);
					validation = true;
					i = list.size();
					
				}
			}
			
			if (clientTrouve == null) {
				validation = false;
				new lanceralerte("Client non trouve", "Le numero doit correspondre au numero d'un client existant");
			}
			
		
		}catch(Exception e) {
			
			validation = false;
			new lanceralerte("Numero invalide", "Le numero entre doit etre un entier");
		}
		 
		
		if (validation == true) {
			if (gestionLocDVD.louerFilm(clientTrouve, filmSelectionne)) {
				
				vueGestionLocations.refreshTableViewFilms();
				
				String string = gestionLocDVD.getListeGestionLocationDVD();
				
				vueGestionLocations.setListeGestionLocationDVD(string);
				
			}
		}
	
		vueGestionLocations.setComboBoxFilm(gestionLocDVD.getObervableListeFilm());
		
		
	}

	
	/**
	 * Cette methode permet de retourner un film.
	 * Un film loue devient donc disponible et la liste des films est actualise.
	 */
	private void retournerLocation() {
		
		int nbClient = 0;
		int nbFilm = 0;
		boolean validation = false;
		String message1 = "";
		String message2 = "";
		String message3 = "";
		try {
		//recuperer le numero du client
		nbClient = Integer.parseInt(vueGestionLocations.getTxtNoClient());
		//recuperer le numero du film
		nbFilm = Integer.parseInt(vueGestionLocations.getTxtNoFilm());
		//retourner le film du client
		
			for (int i = 0; i < gestionLocDVD.getListeClient().size(); i++) {
				
					if (gestionLocDVD.getListeClient().get(i).getNumero() == nbClient) {
						validation = true;
						i = gestionLocDVD.getListeClient().size();
						
					}else {
						validation = false;
						message1 +="Client non trouve\n";
					}
			}
				
			
			for (int j = 0; j < gestionLocDVD.getObervableListeFilm().size(); j++) {
				if (gestionLocDVD.getObervableListeFilm().get(j).getId() == nbFilm) {
					validation = true;
					j = gestionLocDVD.getObervableListeFilm().size();
				}else {
					validation = false;
					message2 += "Film non trouve\n";
				}
					
			}
			
		}catch(Exception e) {
			validation = false;
			message3 += "Veuiller entrer des nombres entiers svp\n";
			
		}
		
		if (validation == false) {
			new lanceralerte("Erreur saisi", message1+message2+message3);
		}else {
			
			gestionLocDVD.retourFilm(nbFilm, nbClient);
			
			vueGestionLocations.setListeGestionLocationDVD(gestionLocDVD.getListeGestionLocationDVD());
			
			vueGestionLocations.refreshTableViewFilms();
		}
		
		
		
	}

	
	/**
	 * Cette methode permet de lire le contenu du fichier Contexte.dat.
	 * Elle permet d'afficher la liste des clients enregistres ainsi que les locations qu'ils ont eu
	 * a effectuer. Elle permet de voir egalement les films disponibles ou loues.
	 */
	private void lireContexte() {
		
//		gestionLocDVD.lireFichierClients();
//		ObservableList<Client> oListClient = FXCollections.observableArrayList(gestionLocDVD.getListeClient());
//		vueGestionClients.getListeDesClientView().setItems(oListClient);
		
		//File file = new File ("res/fichiers/Contexte.dat");
		File file = new File(getClass().getResource(Constantes.NOM_FICHIER_CONTEXTE).getFile());
		
		gestionLocDVD.lireFichierContexte(file);
		
		//System.out.println(gestionLocDVD.getListeGestionLocationDVD());
		vueGestionLocations.setListeGestionLocationDVD(gestionLocDVD.getListeGestionLocationDVD());
		
		vueGestionLocations.refreshTableViewFilms();

		
	}

	/**
	 * Cette methode permet de sauvegarder le contexte de l'application.
	 * Notamment la liste des clients ainsi que les locations qu'ils ont eu a effectuer.
	 */
	private void ecrireContexte() {
		
		String contexte = gestionLocDVD.getContexte();
		gestionLocDVD.ecrireFichierContexte(contexte);
	}


	/**
	 * Cette methode permet de vider les textField des champs de Ajouter Client.
	 */
	private void refreshPartieAjouterClient() {
		
		vueGestionClients.getNomTextField2().setText("");
		vueGestionClients.getPrenomTextField2().setText("");
		vueGestionClients.getHommeButtonRadio2().setSelected(true);
		vueGestionClients.getAgeTextField2().setText("");	
	}
	
	/**
	 * Cette methode permet d'afficher un client une fois que l'on a saisi son numero dans le champ numero client.
	 * Elle montre le nom, le prenom, l'age, le statut et le sexe.
	 */
	private void afficherClient() {
		ArrayList <Client> clientList = gestionLocDVD.getListeClient();
		
		Client clientTrouve = null;
		boolean validation = false;
		String message = "";
		int numeroTape = 0;
		try {
			numeroTape = Integer.parseInt(vueGestionClients.getNumeroDuClientTextField().getText());
			
			for(int i = 0; i < clientList.size(); i++) {
				if(clientList.get(i).getNumero() == numeroTape) {
					clientTrouve = clientList.get(i);
					validation = true;
					i = clientList.size();
				}
			}
			
			if (clientTrouve == null) {
				validation = false;
				message += "Aucun client trouve\n"
						+ "Le numero entre doit correspondre a un numero de client existant";
			}
			
		}catch(Exception e) {
			validation = false;
			message += "Le numero entre doit etre un entier";
		}
		
		if (validation == false) {
			new lanceralerte("Numero Client incorrect", message);
		}
		
		
		//prendre le numero
		vueGestionClients.getNumeroDuClientTextField().setText(Integer.toString(clientTrouve.getNumero()));
		//prendre le nom
		vueGestionClients.getNomTextField1().setText(clientTrouve.getNom());
		//prendre le prenom
		vueGestionClients.getPrenomTextField1().setText(clientTrouve.getPrenom());
		
		//prendre le sexe
		if (clientTrouve.getSexe().equals(Sexe.MASCULIN)) {
			vueGestionClients.getHommeButtonRadio1().setSelected(true);
		}else if (clientTrouve.getSexe().equals(Sexe.FEMININ)) {
			vueGestionClients.getFemmeButtonRadio1().setSelected(true);
		}
		vueGestionClients.getAgeTextField1().setText(Integer.toString(clientTrouve.getAge()));
		
		//prendre le statut.
		if (clientTrouve.getStatus().equals(StatusClient.EN_VIGUEUR)) {
			vueGestionClients.getEnvigueurButtonRadio().setSelected(true);
		}else if (clientTrouve.getStatus().equals(StatusClient.SUSPENDU)) {
			vueGestionClients.getSuspenduButtonRadio().setSelected(true);
		}
	}
	
	/**
	 * Cette methode permet de modifier un client.
	 * Notamment ces attributs.
	 * Une fois que les informations du client sont affichees dans les textFields,
	 * l'utilisateur peut les modifier.
	 */
	private void modifierClient() {
		
		ArrayList <Client> clientList = gestionLocDVD.getListeClient();
		Client clientTrouve = null;
		boolean validation = false;
		String message = "";
		int numeroTape = 0;
		try {
			numeroTape = Integer.parseInt(vueGestionClients.getNumeroDuClientTextField().getText());
			
			for(int i = 0; i < clientList.size(); i++) {
				if(clientList.get(i).getNumero() == numeroTape) {
					clientTrouve = clientList.get(i);
					validation = true;
					i = clientList.size();
				}
			}
			
			if (clientTrouve == null) {
				validation = false;
				message += "Aucun client trouve\n"
						+ "Le numero entre doit correspondre a un numero de client existant";
			}
			
		}catch(Exception e) {
			validation = false;
			message += "Le numero entre doit etre un entier";
		}
		
		if (validation == false) {
			new lanceralerte("Numero Client incorrect", message);
		}else {
			
		//recuperer le nom
		clientTrouve.setNom(vueGestionClients.getNomTextField1().getText());
		//recuperer le prenom
		clientTrouve.setPrenom(vueGestionClients.getPrenomTextField1().getText());
		
		//recuperer le sexe
		RadioButton radioBoutonSelec = (RadioButton) vueGestionClients.getSexeGroup1().getSelectedToggle();
		
		if(radioBoutonSelec.getText().equals("Homme")) {
			clientTrouve.setSexe(Sexe.MASCULIN);
		}else if (radioBoutonSelec.getText().equals("Femme")) {
			clientTrouve.setSexe(Sexe.FEMININ);
		}
		
		//recuperer le statut
		RadioButton radioBoutonSelec2 = (RadioButton) vueGestionClients.getStatutGroup().getSelectedToggle();
		
		if(radioBoutonSelec2.getText().equals("En vigueur")) {
			System.out.println("En vigueur");
			clientTrouve.setEnVigueur();
		}else if (radioBoutonSelec2.getText().equals("Suspendu")) {
			clientTrouve.setSuspendu();
		}
		//recuperer l'age.
		clientTrouve.setAge(Integer.parseInt(vueGestionClients.getAgeTextField1().getText()));
		
		int index = clientTrouve.getNumero() - 1;
		
		//permet de remplacer le client avec les anciens attributs par le nouveau avec les nouveaux attributs.
		gestionLocDVD.getListeClient().set(index, clientTrouve);
		
		//actualiser la listes clients avec les nouveaux attributs du nouveau client.
		vueGestionClients.getLabelClientList().setText(gestionLocDVD.afficherClientList());
		
		//vueGestionClients.getListeDesClientView().getItems().set(index, clientTrouve);
		
		//ecrase et ecrit la nouvelle liste de clients dans le fichier Client.dat
		gestionLocDVD.ecrireFichierClients(gestionLocDVD.getListeClient());
		}
		
		
	}
	
	
	public static void main(String[] args) {
		Application.launch(args);
	}
	
}
