package humain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import enums.Sexe;

public class PersonneTest {
	
	Personne p1, p2, p3, p4;

	@Before
	public void testPersonneStringStringSexeInt() {
		p1 = new Personne ("nom1" , "prenom1", Sexe.MASCULIN, 24);
		p2 = new Personne ("nom2", "prenom2", Sexe.FEMININ, 34);
		p3 = new Personne("", "p", Sexe.MASCULIN, 0);
	}

	@Test
	public void testValiderPersonne() {
		System.out.println(Personne.validerPersonne("", "p", Sexe.MASCULIN));
		assertFalse(Personne.validerPersonne("", "p", Sexe.MASCULIN));
		assertTrue(Personne.validerPersonne("nom2", "prenom2", Sexe.FEMININ));
		
	}
	
	@Test
	public void testSetNom() {
		p3.setNom("nom3");
		assertTrue(p3.getNom() == "nom3");
	}

	@Test
	public void testSetPrenom() {
		p3.setPrenom("prenom3");
		assertTrue(p3.getPrenom() == "prenom3");
	}

	@Test
	public void testValideNomPrenom() {
		assertTrue(Personne.valideNomPrenom(p3.getNom()+p3.getPrenom()));
	}

	@Test
	public void testSetSexe() {
		assertTrue(p1.getSexe() == Sexe.MASCULIN);
		p1.setSexe(Sexe.FEMININ);
		assertTrue(p1.getSexe() == Sexe.FEMININ);
	}

	@Test
	public void testValideSexe() {
		assertTrue(Personne.valideSexe(p1.getSexe()));
	}

	@Test
	public void testGetNom() {
		assertTrue(p1.getNom() == "nom1");
	}

	@Test
	public void testGetPrenom() {
		assertTrue(p1.getPrenom() == "prenom1");
	}

	@Test
	public void testGetSexe() {
		assertTrue(p1.getSexe() == Sexe.MASCULIN);
	}

	@Test
	public void testSetAgeInt() {
		assertTrue(p1.getAge() == 24);
	}

	@Test
	public void testSetAgeString() {
		p2.setAge(34);
		assertTrue(p2.getAge() == 34);
	}

	@Test
	public void testGetAge() {
		
		assertTrue(p2.getAge() == 34);
	}

	@Test
	public void testEqualsObject() {
		assertFalse(p2.equals(p3));
		p3.setNom(p2.getNom());
		p3.setPrenom(p2.getPrenom());
		p3.setAge(p2.getAge());
		p3.setSexe(p2.getSexe());
		assertTrue(p2.equals(p3));
	}


}
