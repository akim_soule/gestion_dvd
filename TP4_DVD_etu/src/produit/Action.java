package produit;

import enums.GenreFilm;
import enums.StatusFilm;

/**
 * Cette classe represente un film Action. Elle herite de la classe Film.
 * Elle a pour principal attribut l'age minimum qui est de 12 ans.
 * @author akimsoule
 * @version 1.0
 *
 */
public class Action extends Film {

	/**
	 * Attribut static de la classe; l'age minimum requis pour regarder un film d'action.
	 */
	public static final int AGE_MIN = 12;
	
	/**
	 * Premier constructeur de la classe utilisant l'id et le nom du film.
	 * @param pId
	 * l'id du film.
	 * @param nom
	 * le nom du film.
	 */
	public Action(int pId, String nom) {
		super(pId, nom);
	}
	
	/**
	 * Deuxieme constructeur de la classe utilisant l'id, le nom et le status du film.
	 * @param pId
	 * l'id du film.
	 * @param pnom
	 * le nom du film.
	 * @param pstatusFilm
	 * le statut du film.
	 */
	public Action(int pId, String pnom, StatusFilm pstatusFilm) {
		super(pId, pnom, pstatusFilm);
	}
	
	/**
	 * @return le genre du film.
	 */
	public GenreFilm getGenre() {
		
		return GenreFilm.ACTION;
	}

	
	/**
	 * @return l'age minimum requis pour regarder le film.
	 */
	public int getAgeMin() {
		return 12;
	}
	
	public static void main(String[] args) {
		Action ob = new Action (9, "Film d'action", StatusFilm.DISPONIBLE);
		
		System.out.println(ob.toString());
	}
	

}