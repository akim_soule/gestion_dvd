package fichier;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;

import app.GestionLocationDVD;
import enums.Sexe;
import enums.StatusFilm;
import humain.Client;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import produit.Action;
import produit.Comedie;
import produit.Horreur;
import produit.Produit;

/**
 * Cette classe permet :
 * - d'ecrire dans le fichier Client.dat,
 * - d'afficher le contenu de Client.dat,
 * - d'ecrire les erreurs dans le fichier error.txt lors de la lecture du fichier Films.txt.
 * - d'afficher le contenu du fichier Films.txt
 * @author akimsoule
 * @version 1.0.
 *
 */
public class FichierFilms {

	public static String lireFile (File file) {
		String ligneLu = "", retour = "";

		try {
			FileReader fluxSource = new FileReader(file);
			BufferedReader bufferSource = new BufferedReader(fluxSource);

			ligneLu = bufferSource.readLine();

			while (ligneLu != null) {
				System.out.println(ligneLu);
				retour += ligneLu;
				ligneLu = bufferSource.readLine();
			}

			bufferSource.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retour;
	}
	
	/**
	 * Permet de lire le contenu du fichier Clients.dat.
	 * @return un arraylist de clients.
	 */
	public static ArrayList<Client> lireFichierClient () {
		
		//File file = new File("res/fichiers/Clients.dat") ;
		FichierFilms obj = new FichierFilms();
		File file = new File (obj.getClass().getResource(Constantes.NOM_FICHIER_CLIENT).getFile());
		ArrayList<Client>lesClientsDeClientDat = new ArrayList<Client>();
		
		try {
			ObjectInputStream oos = new ObjectInputStream(
					new BufferedInputStream(
							new FileInputStream(file)));
			
			boolean continuer = true;
			Client ClientTrouve = null;
			while(continuer){
				try {
					ClientTrouve = (Client)oos.readObject();
					lesClientsDeClientDat.add(ClientTrouve);
				}catch(EOFException e){
					continuer = false;
				}
			}
		oos.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return lesClientsDeClientDat;
	}
	
	/**
	 * Permet de lire le fichier Films.txt et de retourner la liste des films
	 * @param file
	 * Le parametre est le fichier Films.txt.
	 * @return une liste observable de produits.
	 */
	public static ObservableList<Produit> lireFichierFilm(File file) {
			ObservableList<Produit> tabFilm = FXCollections.observableArrayList();

			
			//Ouvrir le fichier
			String ligneLu = "", leTout = "";
			
			
			try {
				
				//Lire du fichier, les lignes de texte une par une jusqu'au fin de fichier
				//File file = new File (new test().getClass().getResource(Constantes.NOM_FICHIER_FILM).getFile());
				
				FileReader fileR = new FileReader(file);
				BufferedReader buffR = new BufferedReader(fileR);
				ligneLu = buffR.readLine();
				
				while(ligneLu != null ){
					
						leTout += ligneLu+"\n";
						
						//D�couper chaque ligne suivant le s�parateur (;): utiliser la fonction split
						String parties [] = ligneLu.split(";");
						
						int donne1 = -1;
						try {
							donne1 = Integer.parseInt(parties[0]);
						}catch(NumberFormatException e){
							//System.err.println(e.toString());
							String erreur = "Erreur no de film : "+e.getMessage();		
							donne1 = -1;
							
							EcrireDansErrorFile(erreur);
						}
						StatusFilm donne2 = null;
						
						if(parties[2].equals("DISPONIBLE")) {
							donne2 = StatusFilm.DISPONIBLE;
						}else {
							donne2 = StatusFilm.LOUE;
						}
						
						Produit obj = null;
						if (parties[3].equals("ACTION")) {
							//Cr�er un objet de type Action ou Comedie ou Horreur selon le type existant de chaque film (dans le fichier Films.txt)	
							obj = new Action(donne1, parties[1], donne2);
							
						}else if (parties[3].equals("COMEDIE")) {
							//Cr�er un objet de type Action ou Comedie ou Horreur selon le type existant de chaque film (dans le fichier Films.txt)	
							obj = new Comedie(donne1, parties[1], donne2);
							
						}else if (parties[3].equals("HORREUR")) {
							//Cr�er un objet de type Action ou Comedie ou Horreur selon le type existant de chaque film (dans le fichier Films.txt)	
							obj = new Horreur(donne1, parties[1], donne2);
							
						}
						
						// Ajouter l'objet cr�e dans la liste tabFilm
						tabFilm.add(obj);
						
						ligneLu = buffR.readLine();
				}
				
				fileR.close();
				buffR.close();
				
				//System.out.println(tabFilm.toString());
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		return tabFilm;
	}
	
	/**
	 * Permet d'ecrire les erreurs trouves lors de la lecture d'un fichier.
	 * @param errorMessage
	 * Le parametre utilise est une chaine de caracteres.
	 */
	public static void EcrireDansErrorFile(String errorMessage) {
		Path path = Paths.get("res/error.txt");
		try
		{
		   Files.write(path, (errorMessage+"\n").getBytes(), StandardOpenOption.CREATE, 
				   StandardOpenOption.WRITE, StandardOpenOption.APPEND);
		}
		catch (IOException exception)
		{
		    System.out.println ("Erreur lors de la lecture : " + exception.getMessage());
		}
	}

	
	/**
	 * Permet d'ecrire dans le fichier Clients.dat la liste des clients sous forme d'objets.
	 * @param clients
	 * Le parametre est une arrayList de clients.
	 */
	public static void ecrireFichierClient(ArrayList<Client> clients) {
		// TODO Auto-generated method stub
		
		FichierFilms obj = new FichierFilms();
		File file = new File(obj.getClass().getResource(Constantes.NOM_FICHIER_CLIENT).getFile());
		
		/**ArrayList<Client> Clientslus = lireFichierClient();
		Clientslus.addAll(clients);**/
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(
					new FileOutputStream(file)));
			
			for(Object ob:clients)
			{
			oos.writeObject(ob);
				
			}

		oos.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
//	public static void main(String[] args) {
//		ArrayList<Client> Boum = lireFichierClient();
//		System.out.println(Boum.toString());
//		/**GestionLocationDVD obj = new GestionLocationDVD();
//		Client Akim = new Client ("Soule", "Akim", Sexe.MASCULIN, 54);
//		Client Samir = new Client ("Sam", "Sour", Sexe.FEMININ, 45);
//		
//		obj.getListeClient().add(Akim);
//		obj.getListeClient().add(Samir);
//		
//		ecrireFichierClient(obj.getListeClient());
//		lireFile(new File("res/fichiers/Clients.dat"));
//		**/
//	}
}
