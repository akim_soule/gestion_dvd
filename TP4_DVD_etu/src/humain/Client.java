package humain;


import enums.Sexe;
import enums.StatusClient;

/**
 * Cette classe represente un client et hérite de la classe personne.
 * Elle a pour principal attribut le numero et le status du client.
 * @author akimsoule
 * @version 1.0
 *
 */
public class Client extends Personne {
	/**
	 * Le parametre qui permet de serialiser l'objet.
	 */
	private static final long serialVersionUID = 2820377048836419618L;

	/**
	 * Le numero du client.
	 */
	private int numero;
	
	/**
	 * Le statut du client.
	 */
	private StatusClient status;

	
	/**
	 * Le parametre nombre de clients qui sera incremente a chaque fois qu'un client sera ajoute.
	 */
	private static int nbClients = 0;


	/**
	 * Le constructeur de la classe qui permet de construire l'objet Client.
	 * @param nom
	 * Le nom du client.
	 * @param prenom
	 * Le prenom du client.
	 * @param sexe
	 * Le sexe du client.
	 * @param age
	 * L'age du client.
	 */
	public Client(String nom, String prenom, Sexe sexe, int age) {
		super(nom, prenom, sexe, age);
		numero = ++nbClients;
		status = StatusClient.EN_VIGUEUR;
	}

	/**
	 * Permet de definir le statut en vigueur a un client.
	 */
	public void setEnVigueur() {
		status = StatusClient.EN_VIGUEUR;
	}

	/**
	 * Permet de definir le statut suspendu a un client.
	 */
	public void setSuspendu() {
		status = StatusClient.SUSPENDU;
	}

	/**
	 * @return le numero du client.
	 */
	public int getNumero() {
		return numero;
	}

	/**
	 * @return le statut du client.
	 */
	public StatusClient getStatus() {
		return status;
	}

	/**
	 * @return la description du client en chaine de caracteres.
	 */
	public String toString() {
		return "#" + numero + ", " + nom + ", " + prenom + "," + sexe + ", " + age + ", " + status;
	}

	/**
	 * Permet de definir le nombre de clients a chaque client.
	 * @param nb
	 * Le parametre est un nombre entier.
	 */
	public void setNbClient(int nb) {
		nbClients = nb;
	}
	
	/**
	 * Permet de verifier l'égalite entre deux objets client.
	 * @return vrai s'ils le sont et faux dans le cas contraire.
	 */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}
	
//	public static void main(String[] args) {
//		
//		Client client1 = new Client ("nom1", "prenom1", Sexe.MASCULIN, 23);
//		client1.setNbClient(9);
//		Client client2 = new Client ("nom2", "prenom2", Sexe.MASCULIN, 24);
//		System.out.println(client2.toString());
//		
//		System.out.println(client2.toString());
//		
//	}
	

}
