package humain;

import enums.Sexe;
import java.io.Serializable;

/**
 * Cette classe represente une personne.
 * Elle a pour principal le nom, le prenom, le sexe et l'age de la personne.
 * @author akimsoule
 * @version 1.0
 *
 */
public class Personne implements Serializable {
	private static final long serialVersionUID = 1L;
	protected String prenom;
	protected String nom;
	protected Sexe sexe;
	protected int age;

	
	public Personne(){}
	
	/**
	 * Constructeur princiapal de la classe permettant de creer un objet Personne.
	 * @param nom
	 * Le nom de la personne.
	 * @param prenom
	 * Le prenom de la personne.
	 * @param sexe
	 * Le sexe de la personnes.
	 * @param age
	 * L'age de la personne.
	 */
	public Personne(String nom,String prenom, Sexe sexe, int age){
		super();
		setNom(nom);
		setPrenom(prenom);
		setSexe(sexe);
		setAge(age);
	}
	
	/**
	 * Permet de valider les attributs de la classe.
	 * @param nom
	 * Le nom de la personne.
	 * @param prenom
	 * Le prenom de la classe.
	 * @param sexe
	 * Le sexe de la personne.
	 * @return vrai si les parametres sont valides et faux dans le cas contraire.
	 */
	public static boolean validerPersonne(String nom, String prenom, Sexe sexe){
		return valideNomPrenom(nom) && valideNomPrenom(prenom) && valideSexe(sexe);
	}

	/**
	 * Modifie le nom. Doit respecter ces criteres (non null, min de 2
	 * caracteres).
	 * @param nom
	 * Le nom du client.
	 * 
	 */
	public void setNom(String nom)  {
		if (valideNomPrenom(nom))
			this.nom = nom;
	}

	/**
	 * Modifie le prenom. Doit respecter ces criteres (non null, min de 2
	 * caracteres).
	 * @param prenom
	 * Le prenom du client.
	 * 
	 */
	public void setPrenom(String prenom) {
		if (valideNomPrenom(prenom))
			this.prenom = prenom;
	}

	/**
	 * Permet de valider le nom et le prenom.
	 * @param nomPrenom
	 * Le nom ou le prenom.
	 * @return vrai s'il est valide et faux dans le cas contraire.
	 */
	public static boolean valideNomPrenom(String nomPrenom) {
		return (nomPrenom != null && nomPrenom.length() >= 2);
	}

	/**
	 * Modifie le sexe. Non null
	 * 
	 * @param sexe
	 *            Sexe de la personne
	 */
	public void setSexe(Sexe sexe) {
		if (valideSexe(sexe))
			this.sexe = sexe;
	}

	/**
	 * 
	 * @param sexe
	 * Le sexe de la personne
	 * @return vrai si le sexe est non nul.
	 */
	public static boolean valideSexe(Sexe sexe) {
		return sexe != null;
	}

	/**
	 * @return le nom de la personne.
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @return le prenom de la personne.
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * @return le sexe de la personne.
	 */
	public Sexe getSexe() {
		return sexe;
	}
	
	/**
	 * Modifie l'age de la personne.
	 * @param age
	 * Le parametre entre est un entier.
	 * 
	 */
	public void setAge(int age){
		this.age = age;
	}
	
	/**
	 * Modifie l'age de la personne.
	 * @param age
	 * Le parametre entre est une chaine de caracteres.
	 */
	public void setAge(String age){
		this.age = Integer.parseInt(age);
	}
	
	/**
	 * @return l'age de la personne.
	 */
	public int getAge(){
		return age;
	}

	/**
	 * Verifie l'egalite entre deux objets personnes.
	 * @param obj
	 * Le parametre obj est compare avec l'objet sur lequel est utilise cette methode.
	 * @return vrai si les objets sont les memes et faux dans le cas contraire.
	 * 
	 */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Personne other = (Personne) obj;
		if (age != other.age)
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		if (sexe != other.sexe)
			return false;
		return true;
	}
}
