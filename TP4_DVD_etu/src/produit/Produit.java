package produit;

import enums.GenreFilm;
import enums.StatusFilm;
/**
 * L'interface Produit implemente par la classe abstraite Film.
 * Elle possedent toutes les fonctions dont aura besoin les classes Film, Action, Comedie et Horreur.
 * @author akimsoule
 * @version 1.0.
 *
 */
public interface Produit {
	public int getId();
	public void setId(int id);
	public String getNom();
	public void setNom(String nom);
	public StatusFilm getStatus();
	public void setStatus(StatusFilm statutFilm);
	public GenreFilm getGenre();
	public void louer();
	public void retour();
	public boolean estDisponible();
	public int getAgeMin();

}
