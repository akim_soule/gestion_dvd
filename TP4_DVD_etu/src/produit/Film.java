package produit;

import enums.StatusFilm;

/**
 * Cette classe represente un film. Elle est abstraite. Elle implemente l'interface Produit.
 * Elle a pour principal attribut l'id, le nom et le statuts.
 * @author akimsoule
 * @version 1.0
 *
 */
public abstract class Film implements Produit {
	
	/**
	 * L'id du film.
	 */
	protected int id;
	/**
	 * Le nom du film.
	 */
	protected String nom;
	/**
	 * Le status du film.
	 */
	protected StatusFilm statusfilm;
	
	
	public Film() {
		
	}
	
	/**
	 * Constructeur permettant de creer un objet d'une classe heritant de la classe Film avec deux parametres..
	 * @param pId
	 * L'Id du film.
	 * @param pNom
	 * Le nom du film.
	 */
	public Film (int pId, String pNom) {
		id = pId;
		nom = pNom;
		
	}
	
	/**
	 * Constructeur permettant de creer un objet d'une classe heritant de la classe Film avec trois parametres.
	 * @param pId
	 * L'id du film.
	 * @param pNom
	 * Le nom du film.
	 * @param pstatutfilm
	 * Le status du film.
	 */
	public Film (int pId, String pNom, StatusFilm pstatutfilm) {
		id = pId;
		nom = pNom;
		statusfilm = pstatutfilm;
	}
	
	/**
	 * @return l'id du film.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Modifie l'id du film.
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return le nom du film.
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Modifie le nom du film.
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	/**
	 * @return le statut du film.
	 */
	public StatusFilm getStatus() {
		return statusfilm;
	}

	/**
	 * Modifie le statut du film.
	 */
	public void setStatus(StatusFilm statusfilm) {
		this.statusfilm = statusfilm;
	}
	
	/**
	 * Permet de transformer le status du film de "DISPONIBLE" a "LOUER".
	 */
	public void louer() {
		if (this.statusfilm.equals(StatusFilm.DISPONIBLE)) {
			this.setStatus(StatusFilm.LOUE);
		}
	}
	
	/**
	 * Permet de transformer le status du film de "LOUER" a "DISPONIBLE".
	 */
	public void retour() {
		if (this.statusfilm.equals(StatusFilm.LOUE)) {
			this.setStatus(StatusFilm.DISPONIBLE);
		}
	}
	
	/**
	 * @return vrai si le film est disponible et faux dans le cas contraire.
	 */
	public boolean estDisponible() {
		boolean ok = false;
		if (this.statusfilm.equals(StatusFilm.DISPONIBLE)) {
			ok = true;
		}
		return ok;
	}

	/**
	 * @return une description du film en chaine de caracteres.
	 */
	public String toString() {
		return "Film [id=" + id + ", nom=" + nom + ", statusfilm=" + statusfilm + "]";
	}
	
	

}
