package fichier;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Cette classe Constantes permet d'acceder au ressources de l'application.
 * Notamment les fichiers Clients.dat, Films.txt, Contexte.dat et icone.png.
 * @author akimsoule
 * @version 1.0.
 *
 */
public class Constantes {

	/**
	 * Le nombre maximum de clients
	 */
	public static final int MAX_CLIENT = 1000;
	
	/**
	 * Le nombre maximum d'employes
	 */
	public static final int MAX_EMPLOYE = 100;
	
	/**
	 * Le nombre maximum de films
	 */
	public static final int MAX_FILM = 1000;

	/**
	 * Les chemins correspondants aux differentes ressources.
	 */
	public static final String NOM_FICHIER_CLIENT = "/fichiers/Clients.dat";
	public static final String NOM_FICHIER_FILM = "/fichiers/Films.txt";
	public static final String NOM_FICHIER_CONTEXTE = "/fichiers/Contexte.dat";
								
	public static final String NOM_ICONE = "/image/icone.png";
	
	public static void main(String[] args) {
		//File file = new File ("res/fichiers/Films.txt");
		
		Constantes c = new Constantes();
		
		
		
		try {
			File file1 = new File (c.getClass().getResource(NOM_FICHIER_FILM).getFile());
			FileInputStream fr = new FileInputStream(file1);
			System.out.println(fr.toString());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
