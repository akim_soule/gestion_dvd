package humain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import enums.Sexe;
import enums.StatusClient;

public class ClientTest {

	Client client1, client2, client3, client1prime;
	
	@Before
	public void testClient() {
		client1 = new Client("nom1", "prenom1", Sexe.MASCULIN, 23);
		client1prime = new Client("nom1", "prenom1", Sexe.MASCULIN, 23);
		client2 = new Client("nom2", "prenom2", Sexe.FEMININ, 25);
		client3 = new Client("nom3", "prenom3", Sexe.MASCULIN, 2);
	}
	
	@Test
	public void testSetEnVigueur() {
		assertTrue(client1.getStatus() == StatusClient.EN_VIGUEUR);
	}

	@Test
	public void testEqualsObject() {
		assertFalse(client1.equals(client2));
		assertTrue(client1.equals(client1prime));
	}
	@Test
	public void testSetSuspendu() {
		assertFalse(client2.getStatus() == StatusClient.SUSPENDU);
		client2.setSuspendu();
		assertTrue(client2.getStatus() == StatusClient.SUSPENDU);
	}

	@Test
	public void testGetNumero() {
		
		assertTrue(client1.getNumero() == 21);
		assertTrue(client2.getNumero() == 23);
		
		assertTrue(client1.getNumero() == 21);
	}

	@Test
	public void testGetStatus() {
		assertTrue(client3.getStatus() == StatusClient.EN_VIGUEUR);
		assertFalse(client3.getStatus() == StatusClient.CREATION);
	}

	@Test
	public void testSetNbClient() {
		client3.setNbClient(4);
		assertTrue(client3.getNumero() == 4);
	}
	
	@Test
	public void testToString() {
		
		String contenu = "#" + client1.getNumero() + 
							", " + client1.getNom() +
							", " + client1.getPrenom() +
							"," + client1.getSexe().toString() + 
							", " + client1.getAge() + ", " +
							client1.getStatus().toString();
		assertTrue(client1.toString().equals(contenu));
		
	}

	

}
