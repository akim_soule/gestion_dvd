package app;

import javafx.scene.paint.Color;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

/**
 * Cette classe correspond a la vue principale de la fenetre application.
 * Elle contient la vue de gestion locations et celle de gestion clients.
 * @author akimsoule.
 * @version 1.0.
 *
 */
public class VuePrincipale {
	
	/**
	 * le root de la vue.
	 */
	private BorderPane root;
	
	/**
	 * la scene de la vue.
	 */
	private Scene scene;
	
	/**
	 * le tabPaneRoot qui permet d'acceuillir les deux vues.
	 */
	private TabPane tabPaneRoot = new TabPane();

	/**
	 * les boutons charger contexte, sauvegarder cntexte et quitter.
	 */
	private Button btnLireContexte;
	private Button btnEcrireContexte;
	private Button btnQuitter;

	/**
	 * les vues gestionLocations et gestionClients qu'on veut ajouter dans le tabPaneRoot.
	 */
	private VueGestionLocations vueGestionLocations;
	private VueGestionClients vueGestionClients;

	/**
	 * Constructeur principal de la vue permettant de mettre en place les deux vues.
	 * @param vueGestionClients
	 * Un objet de la classe vueGestionClients.
	 * @param vueGestionLocations
	 * un objet de la classe vuegestionLocations.
	 * 
	 */
	public VuePrincipale(VueGestionClients vueGestionClients, VueGestionLocations vueGestionLocations) {

		this.vueGestionLocations = vueGestionLocations;
		this.vueGestionClients = vueGestionClients;
		
		root = new BorderPane();
		
		Tab tabGestionLocations = new Tab();
		tabGestionLocations.setText("Gestion des locations");
		tabGestionLocations.setContent(this.vueGestionLocations.getPaneGestionLocation());

		Tab GestionClients = new Tab();
		GestionClients.setText("Gestion des clients");
		GestionClients.setContent(this.vueGestionClients.getPaneGestionClient());
		
		tabPaneRoot.getTabs().addAll(tabGestionLocations, GestionClients);
		tabPaneRoot.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

		root.setCenter(tabPaneRoot);
		scene = new Scene(root, 970, 600, Color.WHITE);

		BorderPane paneEnBas = new BorderPane();
		btnLireContexte = new Button("Charger le contexte");
		btnEcrireContexte = new Button("Sauvegarder le contexte");
		btnQuitter = new Button("Quitter");

		HBox hBoxButton = new HBox();
		hBoxButton.getChildren().addAll(btnLireContexte, btnEcrireContexte, btnQuitter);
		hBoxButton.setPrefSize(400, 0);

		paneEnBas.setRight(hBoxButton);
		paneEnBas.setPadding(new Insets(4));
		root.setBottom(paneEnBas);
	}
	
	/**
	 * Retourne la scene du la vue principale.
	 * @return la scene.
	 */
	public Scene getMainScene() {
		return scene;
	}

	/**
	 * Retourne le bouton charger le contexte.
	 * @return le bouton charger le contexte.
	 */
	public Button getBoutonLireContexte() {
		return btnLireContexte;
	}

	/**
	 * Retourne le bouton ecrire le contexte.
	 * @return le bouton ecrire le contexte.
	 */
	public Button getBoutonEcrireContexte() {
		return btnEcrireContexte;
	}

	/**
	 * Retourne le bouton quitter.
	 * @return le bouton quitter.
	 */
	public Button getBoutonQuitter() {
		return btnQuitter;
	}
}